#!/bin/bash

set -ex

# sudo add-apt-repository --yes ppa:george-edison55/cmake-3.x
# sudo apt-add-repository --yes ppa:fenics-packages/fenics-exp
# sudo apt-get update
# sudo apt-get --yes install git cmake cmake-curses-gui clang-3.6 \
#                            freeglut3-dev libxi-dev libxmu-dev \
#                            liblapack-dev swig3.0 python-dev \
#                            openjdk-7-jdk
# sudo rm -f /usr/bin/cc /usr/bin/c++
# sudo ln -s /usr/bin/clang-3.6 /usr/bin/cc
# sudo ln -s /usr/bin/clang++-3.6 /usr/bin/c++

export PREFIX=$(pwd)/opensim_all_installed

export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
          # -DCMAKE_INSTALL_PREFIX='~/opensim_dependencies_install' \

mkdir -pv opensim_dependencies_build
cd opensim_dependencies_build
cmake ../opensim-core/dependencies/ \
      -DCMAKE_INSTALL_PREFIX="$PREFIX" \
      -DCMAKE_BUILD_TYPE=RelWithDebInfo
make -j6
cd ..

mkdir -pv $PREFIX/bin/
# cp -r $PREFIX/BTK/lib/btk-0.4dev/* $PREFIX/lib/
# cp -r $PREFIX/simbody/libexec/simbody/* $PREFIX/bin/

# NOTE: this works for opensim tag v4.3.1
# ref: https://github.com/opensim-org/opensim-core/issues/3223
search_str='32768 >= MINSIGSTKSZ ? 32768 : MINSIGSTKSZ;'
replace_str='32768;'
grep -rl "${search_str}" opensim-core/ | xargs sed -i "s/${search_str}/${replace_str}/g"

mkdir -pv opensim_build
cd opensim_build
cmake ../opensim-core \
      -DCMAKE_INSTALL_PREFIX="$PREFIX" \
      -DCMAKE_BUILD_TYPE=RelWithDebInfo \
      -DOPENSIM_DEPENDENCIES_DIR="$PREFIX" \
      -DBUILD_PYTHON_WRAPPING=ON \
      -DBUILD_JAVA_WRAPPING=OFF \
      -DPYTHON_VERSION_MAJOR=3 \
      -DPYTHON_VERSION_MINOR=10 \
      -DWITH_BTK=ON
make -j6
ctest -j6
make -j6 install
