# opensim_docker

## quickstart

You will need docker and [dogi](https://github.com/ntorresalberto/dogi) installed.

```bash
git submodule update --init --recursive
./build.sh       # builds the docker image 'opensim'
dogi run opensim # launches the opensim container
./compile.sh     # compiles inside the container
```
