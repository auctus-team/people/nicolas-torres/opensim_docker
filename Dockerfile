FROM ubuntu:22.04

# setup locales
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y locales \
    && rm -rf /var/lib/apt/lists/* \
    && locale-gen "en_US.UTF-8"
ENV LANG=en_US.UTF-8 \
    LANGUAGE=en_US:en \
    LC_ALL=en_US.UTF-8

# add ros repository
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y \
    curl gnupg2 lsb-release gpg wget \
    ccache \
    && curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | apt-key add - \
    && sh -c 'echo "deb [arch=$(dpkg --print-architecture)] http://packages.ros.org/ros2/ubuntu $(lsb_release -cs) main" > /etc/apt/sources.list.d/ros2-latest.list' \
    && rm -rf /var/lib/apt/lists/*
ENV PATH="/usr/lib/ccache:$PATH"


RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    silversearcher-ag git cmake cmake-curses-gui clang \
    freeglut3-dev libxi-dev libxmu-dev coinor-libipopt-dev \
    libtool pkg-config autoconf doxygen graphviz \
    libeigen3-dev liblapack-dev swig4.0 python3-dev python3-numpy \
    openjdk-8-jdk \
    build-essential \
    && apt-get autoremove -y && rm -rf /var/lib/apt/lists/*

# ros-dashing-casadi-vendor \ # for ubuntu 18.04

# for 20.04
# libspdlog-dev libsimbody-dev \

ADD compile.sh /
# RUN /compile.sh
